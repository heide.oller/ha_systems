package ch.fhnw.lifescience;

import static org.junit.Assert.*;

import org.junit.Test;

public class SequenceTest {

	@Test
	public void testCount() {
		String rawData = "ATAAAGG";
		Sequence sut = new Sequence(rawData);
		int count = sut.count("A");		
		assertEquals(" Testing the Nucleotide Count", 4, count);
	}

}
