package ch.fhnw.lifescience;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Main {

    public static void main(String [] args) {
    	Logger LOG = LogManager.getLogger(Main.class);

        try {
        	if (args.length != 1) {
        		LOG.warn("USAGE: java -jar dna.jar INPUT_FASTA_FILE...");
        		System.exit(1);
        	}        		
        	Sequence seq = FastaReader.read(args[0]);
            int totalCount = seq.totalCount();
            LOG.info("Total No of Nucleotide Count = {} ",totalCount);
            int countNucleotideA = seq.count(Sequence.NUCLEOTIDE_A);
            LOG.info("Total Nucleotide Count with Sequence A = {}", countNucleotideA);
            int countNucleotideG = seq.count(Sequence.NUCLEOTIDE_G);
            LOG.info("Total Nucleotide Count with Sequence G = {}",countNucleotideG);
            int countNucleotideC = seq.count(Sequence.NUCLEOTIDE_C);
            LOG.info("Total Nucleotide Count with Sequence C = {}", countNucleotideC);
            int countNucleotideT = seq.count(Sequence.NUCLEOTIDE_T);
            LOG.info("Total Nucleotide Count with Sequence T = {}", countNucleotideT);            
            int totalLengthOfDNASeq = seq.totalLengthOfDNASequence();
            LOG.info("Total No of Non-Nucleotide Count = {} ", (totalLengthOfDNASeq-totalCount));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
