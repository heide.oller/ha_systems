package ch.fhnw.lifescience;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DNAUtil {

    /**
     * Returns a list of indices, where an interesting window starts
     * @param seq DNA Sequence
     * @param windowSize Size of the window
     * @param lowerLimit (0-1) If the number of one amino acid is lower than lowerLimit*windowSize, it is an interesting window
     * @param upperLimit (0-1) If the number of one amino acid is higher than upperLimit*windowSize, it is an interesting window
     * @return as described
     */
    public static List<Integer> analyze(Sequence seq, int windowSize, float lowerLimit, float upperLimit) {
        List<Integer> result = new ArrayList<>();

        return result;
    }
}
