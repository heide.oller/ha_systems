package ch.fhnw.lifescience;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Sequence {
	
	static Logger LOG = LogManager.getLogger(Sequence.class);
	
    public static final String NUCLEOTIDE_A = "A";
    public static final String NUCLEOTIDE_C = "C";
    public static final String NUCLEOTIDE_G = "G";
    public static final String NUCLEOTIDE_T = "T";

    private String data;

    public Sequence(String seq) {
        this.data = seq;
    }

    public String getRawData() {
        return data;
    }

     /**
     * Returns the amount of a given nucleotide.
     * @param nucleotide
     * @return as described
     */
    public int count(String nucleotide) {
        int result = 0;
        // YOUR CODE
        char[] charArrayDnaSequence = data.toCharArray();
		for (int i = 0; i < charArrayDnaSequence.length; i++) {
			if (nucleotide.equals(Character.toString(charArrayDnaSequence[i]))) {
				result++;
			}
		}        
		return result;
    }
    /**
     * Returns a map with the count of all nucleotide.
     * @return as described
     */
    public Map<String, Integer> countAll() {
        Map<String, Integer> result = new HashMap<>();

        // YOUR CODE

        return result;
    }
    
    /**
     * Returns a count of all nucleotide.
     * @return as described
     */
    public int totalCount() {
    	int result = 0;
        // YOUR CODE
        for (Character charArrayDnaSequence : data.toCharArray()){
        	if (NUCLEOTIDE_A.equals(charArrayDnaSequence.toString())
					|| NUCLEOTIDE_C.equals(charArrayDnaSequence.toString())
					|| NUCLEOTIDE_G.equals(charArrayDnaSequence.toString())
					|| NUCLEOTIDE_T.equals(charArrayDnaSequence.toString()) ){
				result++;
			}
        }
        return result;
    }

    public int totalLengthOfDNASequence() {
        return data.length();
    }
    
    
}
