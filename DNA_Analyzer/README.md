# DNA Analyser Software README
Version 1.0 HA_Systems

Updated: 01.04.2020

DNA Analyser Software is intended for the reading of FASTA files containing DNA sequences (ATCG).

##Installation

Use the software with the bash command line:

```bash
...\dist>java -jar dna_1.0_testing.jar
```

##Functions
The current version of DNA Analyzer is able to count the number of A, C, G, T bases located in a FASTA sequence.